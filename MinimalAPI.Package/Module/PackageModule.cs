﻿using Dapper;
using System.Data.SqlClient;

namespace MinimalAPI.Package.Module
{
    public class PackageModule : ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            app.MapPost("/packages", async (SqlConnection db, RegisterPackageDTO dto) =>
            {
                var newPackage = await db.QueryFirstOrDefaultAsync<RegisterPackageDTO>
                (
                    @"insert into package(code, country, description)
                      output inserted.*
                      values(@code, @country, @description)", dto
                    );

                return Results.Created($"/package/{newPackage.PackageId}", newPackage);
            });


            app.MapGet("/package", async (SqlConnection db) =>
            {
                try
                {
                    var packages = await db.QueryAsync("select * from package");

                    if (packages != null || packages.ToList().Count == 0)
                    {
                        return Results.Ok(packages);
                    }
                }
                catch (Exception ex)
                {
                    return Results.NotFound("Mensagem: " + ex.Message + ". Provavelmente não encontramos nenhum package.");
                }

                return Results.BadRequest("Algum erro foi detectado. Por favor report ao administrador");
            });


            app.MapGet("/package/{code}", async (SqlConnection db, string code) =>
            {
                try
                {
                    var package = await db.QueryFirstAsync("select * from package where code = @code", new { code });

                    if (package != null)
                    {
                        return Results.Ok(package);
                    }
                }
                catch (Exception ex)
                {
                    return Results.NotFound("Mensagem: " + ex.Message + $". Provavelmente não encontramos seu package com code igual a {code}.");
                }

                return Results.BadRequest("Algum erro foi detectado. Por favor report ao administrador");
            });

            //app.MapGet("/package", async (SqlConnection db) =>
            //    await db.QueryAsync("select * from package"));

            //app.MapGet("/package/{code}", async (SqlConnection db, string code) =>
            //    await db.QueryFirstAsync("select * from package where code = @code", new {code}));

        }
    }
}
